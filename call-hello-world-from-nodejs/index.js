import { Context } from '@extism/extism';
import { readFileSync } from 'fs';

let ctx = new Context();
let wasm = readFileSync('../hello-world/hello-world.wasm');
let p = ctx.plugin(wasm, true);

if (!p.functionExists('helloWorld')) {
  console.log("no function 'helloWorld' in wasm");
  process.exit(1);
}

let buf = await p.call('helloWorld', "😀 Hey!"); // if I use emoji, the output is truncated: { message: '👋 Hello World 🌍', input: '😀 He' }
console.log(JSON.parse(buf.toString()));
p.free();
