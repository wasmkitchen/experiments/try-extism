import { Context } from '@extism/extism';
import { readFileSync } from 'fs';

let ctx = new Context();
let wasm = readFileSync('../factorial-rust/target/wasm32-wasi/release/factorial.wasm');
let p = ctx.plugin(wasm, true);

if (!p.functionExists('factorial')) {
  console.log("no function 'factorial' in wasm");
  process.exit(1);
}

const start = Date.now();

let buf = await p.call('factorial', '{"value":30}'); // if I use emoji, the output is truncated: { message: '👋 Hello World 🌍', input: '😀 He' }
console.log(JSON.parse(buf.toString()));

const end = Date.now();
console.log(`Execution time: ${end - start} ms`);


p.free();
