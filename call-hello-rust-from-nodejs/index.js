import { Context } from '@extism/extism';
import { readFileSync } from 'fs';

let ctx = new Context();
let wasm = readFileSync('../hello-rust/target/wasm32-wasi/release/hello_rust.wasm');
let p = ctx.plugin(wasm, true);

if (!p.functionExists('add')) {
  console.log("no function 'add' in wasm");
  process.exit(1);
}

let buf = await p.call('add', '{"a":30,"b":12}'); // if I use emoji, the output is truncated: { message: '👋 Hello World 🌍', input: '😀 He' }
console.log(JSON.parse(buf.toString()));
p.free();
