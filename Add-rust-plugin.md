

```bash
cargo new --lib hello-rust
cd hello rust
```

Update `Cargo.toml`

Include this:
```toml
[lib]
crate_type = ["cdylib"]
```

Install extism dependency:
```bash
cargo add extism-pdk
```
