#![no_main]

use extism_pdk::*;

#[derive(serde::Deserialize)]
struct Parameters {
    value: u128
}

#[derive(serde::Serialize)]
struct Results  {
    value1: u128,
    value2: u128,
    value3: u128,
}

fn f(n: u128) -> u128 {
    if n < 2 {
        1
    } else {
        n * f(n - 1)
    }
}

#[plugin_fn]
pub fn factorial(Json(parameters): Json<Parameters>) -> FnResult<Json<Results>> {

    let factorial = Results { 
        value1: f(parameters.value),
        value2: f(parameters.value + 10),
        value3: f(parameters.value + 20),
     };

    Ok(Json(factorial))
}
